import MySQLdb as mariadb
import json
import requests
def main(topic,srcSBName,destSBName,destBrokerName,destTopic,**kwargs):
    res = "Unable to request"
    try:
        #designed to be very simple, there are 2 things to do
        #1. validate the destination of rewire function
        #2. validate the source or rewire function, either to do validation of given source SB or validation and discovery of the given source SB
        
        headers = {'content-type':'application/json'}
        #1        
        r = requests.post( 'http://10.8.0.2:7070/v1/executions/' ,headers=headers, json={"function_id":"a0fcc435-6f47-4883-bcc3-b50d715add21","input":"{\"destSBName\":\""+destSBName+"\",\"destBrokerName\":\""+destBrokerName+"\",\"destTopic\":\""+destTopic+"\"}"})
        ro = r.json()['result']
        roj = json.loads(ro)    
        destValidationRes = roj['output']        
        destValidationResj = json.loads(destValidationRes)
        if destValidationResj['res'] == 0:
            destSBIP=destValidationResj['destSBIP']
            destBrokerName=destValidationResj['destBrokerName']
            destTopic=destValidationResj['destTopic']
            res = "Destination invalid"
            if destSBIP and destBrokerName and destTopic:
                #2
                r = requests.post( 'http://10.8.0.2:7070/v1/executions/' ,headers=headers, json={"function_id":"4874a392-cd03-472f-b533-88b427bce1b7","input":"{\"srcSBName\":\""+srcSBName+"\",\"destSBName\":\""+destSBName+"\",\"Topic\":\""+topic+"\"}"})
                ro = r.json()['result']
                roj = json.loads(ro)    
                srcValidationRes = roj['output']        
                srcValidationResj = json.loads(srcValidationRes)
                if srcValidationResj['res'] == 0:
                    srcSBIP=srcValidationResj['srcSBIP']
                    Topic=srcValidationResj['Topic']
                    #call the forwarding function of the SB
                    r = requests.post( 'http://'+srcSBIP+':9001/requestDataForwarding' ,headers=headers, json={"Topic":Topic,"DestSBIP":destSBIP,"DestBrokerName":destBrokerName,"DestTopic":destTopic})
                    ro = r.text
                    roj = json.loads(ro)
                    resultBrokerName = roj['res']
                    res = resultBrokerName
                
        
    except Exception as e:
        res = str(e)        
       
    return str(res)
   
if __name__ == '__main__':
    main()
