from flask import Flask, request
from flask_restful import Api, Resource, reqparse
import requests

import sqlite3
from sqlite3 import Error

import json

import socket
import fcntl
import struct

from time import sleep
import threading

from autobahn.twisted.wamp import *
from autobahn.wamp.exception import ApplicationError

from autobahn.twisted.wamp import Application
from autobahn.wamp.types import SubscribeOptions

app = Flask(__name__)
api = Api(app)

async_result = ""
subs =  []

global_data = None



class RequestNF(Resource):

    def post(self):
        data = request.get_json()        
        #topic that superbroker is interested in
        topicOfInterest = data["Topic"]                 
        global subs 
        subs.append(topicOfInterest)        
        createVirtualSBSubscription( topicOfInterest)        
        return 200
        
class RequestP(Resource):

    def post(self):
        data = request.get_json()        
        #topic that superbroker want to publish on broker
        topicToPublish = data["Topic"]
        #data to publish
        dataToPublish = data["data"]        
        publishOnBroker(topicToPublish,dataToPublish)        
        return 200

def createVirtualSBSubscription(param):
    try:
        #save the rewireId in subs; rewireId is param[1], topic is param[0];
        global subs
        if param in subs:
            topic = param            
            appl = async_result.get()            
            appl.session.subscribe(sendRewireDataToSB, topic, options=SubscribeOptions(match=u'exact', details_arg='details'))
    except Error as e:
        print("Error: ", e)

 
def sendRewireDataToSB(*args, **kwargs):    
    urlSB = "http://" + ipSB + ":9001/receiveRewireData"    
    data = args    
    Topic = kwargs["details"].topic    
    parametersBroker = {"data": data, "Topic": Topic}    
    r = requests.post(urlSB, params=parametersBroker)
    return 200
    

def publishOnBroker(topicToPublish,dataToPublish):
    #Connection to the broker     
    appl2 = async_result.get()   
    appl2.session.publish(topicToPublish, dataToPublish)
    

def createAppl():
    sleep(5)
    appl = Application()
    appl.run(url="ws://localhost:8181/ws", realm="s4t", start_reactor=False)
    return appl

def subMetaAPI(asyn):
    sleep(15)
    print("====1====")
    appl2 = async_result.get()


ipSB = "10.8.0.10"
brokerNameAttuale = "Broker1D1"
#Util function for registering a broker on the superBroker
def registrationBroker():    
    urlSB = "http://" + ipSB + ":9001/registerBroker"    
    parametriBroker = {"BrokerName": brokerNameAttuale, "IPBroker": ipaddr}    
    r = requests.post(urlSB, params=parametriBroker)    
    domainJson = json.loads(r.text)
    global domain
    domain = domainJson["domain"]    
    return 200

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15].encode('utf-8'))
    )[20:24])
	

ipaddr = get_ip_address('tun0')



def inizio():    
    #register the broker on the SuperBroker
    registrationBroker()
    print("Broker registration is done! Returned Domain name is: ",domain, " about to start API server on 9002")
    #Start the API server
    thr = threading.Thread(target=server, args=(), kwargs={})
    thr.start()

    #Create a session to the broker
    from multiprocessing.pool import ThreadPool
    pool = ThreadPool(processes=1)
    global async_result
    async_result = pool.apply_async(createAppl, ())

    thr2 = threading.Thread(target=subMetaAPI, args=(async_result, ), kwargs={})    
    thr2.start()

    sleep(1)

def server():    
    #api.add_resource(Request, "/requestData")
    #api.add_resource(RequestF, "/requestDataForwarding")
    api.add_resource(RequestNF, "/requestDataForwardingN")
    #api.add_resource(RequestFS, "/requestDataForwardingStop")
    api.add_resource(RequestP, "/requestDataPublishing")    
    
    app.run(host='0.0.0.0', port='9002')

topics = set()
domain = ""
def addToTopics(topic):    
    #make sure the topic for the domain
    splitedToken = topic.split('.')
    if splitedToken[0] == domain:
        #we can add the topic to our set if not in the set
        if topic not in topics:
            #set new topic to the superbroker
            sendTopicToSB(topic)
            topics.add(topic)

def sendTopicToSB(topic):
    urlSB = "http://" + ipSB + ":9001/registerBrokerTopic"    
    parametersBroker = {"BrokerName": brokerNameAttuale, "Topic": topic}    
    r = requests.post(urlSB, params=parametersBroker)
    return 200
