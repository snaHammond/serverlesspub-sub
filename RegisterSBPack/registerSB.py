import MySQLdb as mariadb
def main(ipSuperBroker,superBrokerName,superBrokerDomain,**kwargs):
    res = ""
    try:
        db = mariadb.connect(host="192.168.56.104",    
                         user="federator",        
                         passwd="password",  
                         db="feddb")        
        cur = db.cursor()
        query = 'SELECT name FROM superbrokers WHERE name = "'+ superBrokerName +'"'
        cur.execute(query)
        listSB = cur.fetchall()
        if not listSB:
            #this broker has not been registered so do insertion
            query = 'INSERT IGNORE INTO superbrokers (ip, name,domain) VALUES ("'+ ipSuperBroker +'", "'+ superBrokerName +'", "'+ superBrokerDomain +'")'
        else:
            #this broker has been already registered so update
            SB = listSB[0]
            superBrokerName = SB[0]
            query = 'UPDATE superbrokers SET ip = "'+ ipSuperBroker +'", domain = "'+superBrokerDomain+'"  WHERE name = "'+ superBrokerName +'"'
        
	cur.execute(query)
	db.commit()
        
        res = "SuperBroker Registration Completed Successfully"
    except Exception as e:
        res = str(e)
    finally:
        db.close()
       
    return str(res)
   
if __name__ == '__main__':
    main()
