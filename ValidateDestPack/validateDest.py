import MySQLdb as mariadb
import json
import requests

def getSBIP(SBName):
    try:
        db = mariadb.connect(host="192.168.56.104",    
                         user="federator",        
                         passwd="password",  
                         db="feddb")
        cur = db.cursor()
        query = 'select ip from superbrokers where name = "'+ SBName +'"'
        cur.execute(query)
        listSB = cur.fetchall()
        if listSB:
            SB = listSB[0]
            code = 0
            SBIP = str(SB[0])
            res = "{\"res\":"+str(code)+",\"SBIP\":\""+SBIP+"\"}"
    except Exception as e:
        code = -2
        SBIP = ''
        res = "{\"res\":"+str(code)+",\"SBIP\":\""+SBIP+"\"}"
    finally:
        db.close()
        
    return res
      
def getSBDomain(SBName):  
    try:
        db = mariadb.connect(host="192.168.56.104",    
                         user="federator",        
                         passwd="password",  
                         db="feddb")
        cur = db.cursor()
        query = 'select domain from superbrokers where name = "'+ SBName +'"'
        cur.execute(query)
        listSB = cur.fetchall()
        if listSB:
            SB = listSB[0]
            code = 0
            SBDomain = str(SB[0])
            res = "{\"res\":"+str(code)+",\"SBDomain\":\""+SBDomain+"\"}"            
    except Exception as e:
        code = -2
        res = "{\"res\":"+str(code)+",\"SBDomain\":\""+SBDomain+"\"}" 
    finally:
        db.close()
       
    return str(res)
        
def main(destSBName,destBrokerName,destTopic,**kwargs):
    code = -1
    destSBIP = ''
    destBrokerNameV = ''
    destTopicV = ''
    res = "{\"res\":"+str(code)+",\"destSBIP\":\""+destSBIP+"\",\"destBrokerName\":\""+destBrokerNameV+"\",\"destTopic\":\""+destTopicV+"\"}"
    #first basic check if variables are filled
    if destSBName and destBrokerName and destTopic:
        #get the destSB ip        
        headers = {'content-type':'application/json'}
        """
        r = requests.post( 'http://10.8.0.2:7070/v1/executions/' ,headers=headers, json={"function_id":"4f9ad447-a485-422c-bf6c-4e6d4b07a257","input":"{\"SBName\":\""+destSBName+"\"}"})
        ro = r.json()['result']
        roj = json.loads(ro) 
        """    
        resultsIP = getSBIP(destSBName)        
        ##get the ip, have to unmarshall the reponse from getSBIP
        resultsIPj = json.loads(resultsIP)   
        if resultsIPj['res']==0:
            code = 0
            destSBIP = resultsIPj['SBIP']
            
            res = "{\"res\":"+str(code)+",\"destSBIP\":\""+destSBIP+"\",\"destBrokerName\":\""+destBrokerNameV+"\",\"destTopic\":\""+destTopicV+"\"}"
            #validate the destination broker name, call the super broker and ask if the broker is located
            r = requests.post( 'http://'+destSBIP+':9001/checkBroker/' ,headers=headers, json={"BrokerName":destBrokerName})
            ro = r.text
            roj = json.loads(ro)
            resultBrokerName = roj['res']
            if resultBrokerName == 0:
                destBrokerNameV = destBrokerName
                res = "{\"res\":"+str(code)+",\"destSBIP\":\""+destSBIP+"\",\"destBrokerName\":\""+destBrokerNameV+"\",\"destTopic\":\""+destTopicV+"\"}"
                #the destbroker is on the destSB so we have to now validate the topic
                #1. check if the topic's first class is the domain of the destSB
                """
                r = requests.post( 'http://10.8.0.2:7070/v1/executions/' ,headers=headers, json={"function_id":"023fae17-8320-493e-84c4-67ca6924a122","input":"{\"SBName\":\""+destSBName+"\"}"})
                ro = r.json()['result']
                roj = json.loads(ro)    
                """
                resultsDomain = getSBDomain(destSBName)
                resultsDomainj = json.loads(resultsDomain)
                if resultsDomainj['res'] == 0:
                    destSBDomain = resultsDomainj['SBDomain']   
                    #2. check if the topic is no already on the SBIP
                    topicSplitted = destTopic.split(".")
                    if topicSplitted[0] == destSBDomain:
                        r = requests.post( 'http://'+destSBIP+':9001/checkTopic' ,headers=headers, json={"topic":destTopic})
                        ro = r.text
                        roj = json.loads(ro)
                        resultBrokerName = roj['res']
                        if resultBrokerName != 0:
                            #Topic does not exist on any broker in the domain so we can go ahead and subscribe for it
                            destTopicV = destTopic
                            res = "{\"res\":"+str(code)+",\"destSBIP\":\""+destSBIP+"\",\"destBrokerName\":\""+destBrokerNameV+"\",\"destTopic\":\""+destTopicV+"\"}"
                    
           
       
    return str(res)
   
if __name__ == '__main__':
    main()