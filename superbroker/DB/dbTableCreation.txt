CREATE TABLE contacts
( contact_id INT(11) NOT NULL AUTO_INCREMENT,
  last_name VARCHAR(30) NOT NULL,
  first_name VARCHAR(25),
  birthday DATE,
  CONSTRAINT contacts_pk PRIMARY KEY (contact_id)
);


CREATE TABLE IF NOT EXISTS brokers (broker_id INT(11) NOT NULL AUTO_INCREMENT,ip varchar(15), name varchar(100), UNIQUE (ip, name), CONSTRAINT brokers_pk PRIMARY KEY (broker_id))