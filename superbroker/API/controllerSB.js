var express = require("express");
var request = require('request');
var mariadb = require('mariadb');
var util = require('util')

//create an api server using express
var api = express();

//api server runs on port 9001
api.listen(9001, () => {
 	console.log("Server working on the port 9001");
});
api.use(express.json());

//create a db connection object
var connection = mariadb.createPool({ host: '127.0.0.1', user:'superbroker', password:'password', database: 'sbdb' });

//create broker table if it does not exist
connection.query("CREATE TABLE IF NOT EXISTS brokers (broker_id INT(11) NOT NULL AUTO_INCREMENT,ip varchar(15), name varchar(100), UNIQUE (ip, name), CONSTRAINT brokers_pk PRIMARY KEY (broker_id))")
	.catch(err => {
		console.log("Impossible to create table broker, error : " + err);
});


//create topics table if it does not exist
connection.query("CREATE TABLE IF NOT EXISTS topics (topic_id INT(11) NOT NULL AUTO_INCREMENT,broker_id INT(11), topic varchar(100), UNIQUE (broker_id, topic), CONSTRAINT topics_pk PRIMARY KEY (topic_id))")
	.catch(err => {
		console.log("Impossible to create table topics, error : " + err);
});

//create rewire request table
connection.query("CREATE TABLE IF NOT EXISTS rewire_requests (id INT(11) NOT NULL AUTO_INCREMENT, destSBIP varchar(40),destBrokerName varchar(40), Topic varchar(40), destTopic varchar(40), "+
					"UNIQUE(Topic,destSBIP,destBrokerName), CONSTRAINT rewire_requests_pk PRIMARY KEY (id))")
  	.catch(err => {
		console.log("Impossible to create table rewire_requests, error : " + err);
});


//fed IP
var fedIp = "10.8.0.2";
var SuperBrokerNameV = "SB1";
var IPSuperBrokerV = "10.8.0.10";
var SuperBrokerDomainV = "domain1";

//register itself on the federator
var fedUrl = "http://" + fedIp + ":7070/v1/executions/";
var parameters = { SuperBrokerName:SuperBrokerNameV, IPSuperBroker:IPSuperBrokerV, SuperBrokerDomain:SuperBrokerDomainV };
var parameters = {
  "function_id":"60a16cf5-2ccd-4cb0-9e18-64dca9550331",
   "input":"{\"ipSuperBroker\":\""+IPSuperBrokerV+"\",\"superBrokerName\":\""+SuperBrokerNameV+"\",\"superBrokerDomain\":\""+SuperBrokerDomainV+"\"}"
}
request({url:fedUrl, method:"POST", json:true, body:parameters}, function(err, response, body) {
	if(err) { console.log(err); return; }
	console.log("Response: " + response.statusCode);
});

//Forward data from a specific broker to another broker
api.post("/requestDataForwarding", (req, res) => {

var topicOfInterest = req.body.Topic;
var DestSBIP = req.body.DestSBIP;
var DestBrokerName = req.body.DestBrokerName;
var DestTopic = req.body.DestTopic;

/*
var ipBXOrigine, ipBXDestinazione;
*/
console.log("Fed needs data from: " + topicOfInterest + "to be re-routed to the Super broker with IP: " + DestSBIP + ". The dest broker is  " + DestBrokerName + " with topic " + DestTopic);

//use the topic; topicOfInterest, to find the broker details (ip)
var brokerIP =null;
connection.query('select ip from brokers,topics where brokers.broker_id = topics.broker_id and topics.topic="'+topicOfInterest+'"')
	.then((rows) => {		
			
		if(rows[0]!== undefined && rows[0]!==null ){
			brokerIP = rows[0].ip;
			//save the rewire request			
			var query = 'INSERT INTO rewire_requests (Topic,destSBIP,destBrokerName,destTopic) VALUES ("'+ topicOfInterest +'", "'+ DestSBIP +'", "'+ DestBrokerName +'", "'+ DestTopic +'")';
			connection.query(query)
				.then((resQuery) => {
					
					console.log("Rewire request has successfully been registered!");
					//check if we have already subscribed for it, if yes (freq > 1), we dont call requestDataForwardingN, if no we call requestDataForwardingN
					query = 'select count(*) as freq from rewire_requests where Topic= "'+ topicOfInterest +'"';
					connection.query(query)
						.then((rows) => {
							var freq = rows[0].freq;
							
							if(freq>1){
								return res.json({"res":"Request completed Freq"});
							} else{
								//now do the request for data
								var originUrl = "http://" + brokerIP+ ":9002/requestDataForwardingN";							
								var parameters = { Topic:topicOfInterest };
								request({url:originUrl, method:"POST", json:true, body:parameters}, function(err, response, body) {
									if(err) { 
										console.log(err); 
										return res.json({"res":"Could not request for data"});
									}
									console.log("response: " + response.statusCode);
									if(response.statusCode == 200){
										return res.json({"res":"Request completed"});
									}else{
										return res.json({"res":"Rewire request made to broker but broker could not handle it"});
									}																		
								});
							}
						})
						.catch(err => {
							console.log("Impossibile eseguire la query:" + err);
							return res.json({"res":"Please try again later rewire request id could not be found"})
						});							
					
					
				})
				.catch(err => {
					console.log("Error query:" + err);
					return res.json({"res":"Please try later, (Cannot insert into rewire request Maybe a duplicated request)"});										
				});	
		}else{
			return res.json({"res":"No Available Broker with topic: "+topicOfInterest});
		}
						  
	})
	.catch(err => {
			console.log("Imposibile to run the query:" + err);
			return res.json({"res":"Please try later, (Cannot get brokerIP ask programmer to check database state, table; broker,topics)"});
	});

});


api.post("/registerBroker", (req, res) => {
	var brokerName = req.query.BrokerName;
	var ipBroker = req.query.IPBroker;
	var update;
	var query;
	var brokerNameDb = null;

	console.log("the broker '"+ brokerName +"' with IP '"+ipBroker+"' has requested to be registered on the super broker");

	connection.query('SELECT name FROM brokers WHERE name = "'+ brokerName +'"')
	  	.then(rows => {	
			try{
				brokerNameDb = rows[0].name;
			}catch(err){
				update = 0;
			}
			if(brokerNameDb != null)
			 update = 1;

			if (update == 1) {
				console.log("Update");
				query = 'UPDATE brokers SET ip = "'+ ipBroker +'" WHERE name = "'+ brokerName +'"';
			} else {
				console.log("Insert");
				query = 'INSERT IGNORE INTO brokers (ip, name) VALUES ("'+ ipBroker +'", "'+ brokerName +'")';
			}
			connection.query(query)
			  .then(() => {
				var query = 'select broker_id from brokers where name= "'+ brokerName +'"';
				connection.query(query)
					.then((rows) => {
						var broker_id = rows[0].broker_id;	
						//delete all request for the broker
						var query = 'DELETE FROM rewire_requests WHERE Topic IN (select topic from topics where broker_id ='+ broker_id + ')';
						connection.query(query)
							.then(() => {
								//delete all topics for the broker
								var query = 'DELETE FROM topics WHERE broker_id=' + broker_id;
								connection.query(query)
									.then(() => {
										return;										
									})
									.catch(err => {
										console.log("Impossibile eseguire la query:" + err);										
									})
							})
							.catch(err => {
								console.log("Impossibile eseguire la query:" + err);								
							});						
					})
					.catch(err => {
						console.log("Impossibile eseguire la query:" + err);						
					})
				console.log("Broker has sucessfully been registered!");		
				return res.json({"domain":SuperBrokerDomainV});
			  })
			  .catch(err => {
				console.log("Error query:" + err);
			  });			  
		})
		.catch(err => {
			console.log("Impossibile eseguire la query:" + err);
		});
});

api.post("/checkBroker", (req, res) => {
var brokerName = req.body.BrokerName;

console.log("checking if the broker "+ brokerName +" exits");

connection.query('SELECT name FROM brokers WHERE name = "'+ brokerName +'"')
  .then(rows => {
		try{
			var brokerNameDb = rows[0].name;
			if( brokerNameDb ) {
				return res.json({"res":0});
			}else{
				return res.json({"res":-1});
			}
		}catch(err){
			console.log(err)
			return res.json({"res":-2});
		}
	});
});


api.post("/checkTopic", (req, res) => {
var topic = req.body.topic;

console.log("checking if the topic "+ topic +" exits");

connection.query('SELECT topic FROM topics WHERE topic = "'+ topic +'"')
  .then(rows => {
		try{
			var topicDb = rows[0].topic;
			if( topicDb ) {
				return res.json({"res":0});
			}else{
				return res.json({"res":-1});
			}
		}catch(err){
			return res.json({"res":-2});
		}
	});
});

api.post("/registerBrokerTopic", (req, res) => {
	var brokerName = req.query.BrokerName;
	var topic = req.query.Topic;
	var insert;
	var query;
	var brokerId = null;

	console.log("the broker '"+ brokerName +"' has a new publication topic'"+topic+"' and wants to register it");

	connection.query('SELECT broker_id FROM brokers WHERE name = "'+ brokerName +'"')
		.then((rows) => {			
			brokerId = rows[0].broker_id;			
			if(brokerId != null){
				query = 'INSERT IGNORE INTO topics (broker_id, topic) VALUES ("'+ brokerId +'", "'+ topic +'")';
				connection.query(query)
					.then(() => {
						console.log("Topic for broker has sucessfully been registered!");		
						return res.json();
					})
					.catch(err => {
						console.log("Error query:" + err);
					});
			} 
		})
	  	.catch(err => {
			console.log("Impossibile eseguire la query:" + err);
		})
});


//receiveRewireData used to received data that needs to be re-routered (send to the destSB)
api.post("/receiveRewireData", (req, res) => {
	var data = req.query.data;
	var Topic = req.query.Topic;

	//we need to send to all locations that have requested rewiring
		//get all the rewire requests of that topic to get their destinations 
		var query = 'select destSBIP,destBrokerName,destTopic from rewire_requests where Topic= "'+ Topic +'"';
		connection.query(query)
			.then((rows) => {
				var i;					
				for (i = 0; i < rows.length; i++) {						
					var destSBIP = rows[i].destSBIP;
					var destBrokerName = rows[i].destBrokerName;
					var destTopic = rows[i].destTopic;
					//now send the received data to the destSBI
					var destSBUrl = "http://" + destSBIP+ ":9001/receiveRewireDataFromSB";							
					var parameters = { BrokerName:destBrokerName, Topic:destTopic, data:data};					
					
					request({url:destSBUrl, method:"POST", json:true, body:parameters}, function(err, response, body) {
						if(err) { 
							console.log(err); 
							return res.json({"res":"Could not forward data to destSB"});
						}
						console.log("response: " + response.statusCode);
						if(response.statusCode == 200){								
							return;
						}else{
							return;
						}																		
					});
					
				}
				
			})
			.catch(err => {
				console.log("Impossibile eseguire la query receiveRewireData :" + err);
				
			})	
	

return res.json();
});


//receiveRewireDataFromSB used to received data that needs to be re-routered, from a colleague SB
api.post("/receiveRewireDataFromSB", (req, res) => {
	var BrokerName = req.body.BrokerName;
	var Topic = req.body.Topic;
	var data = req.body.data;

	var query = 'select ip from brokers where name= "'+ BrokerName +'"';
	connection.query(query)
		.then((rows) => {
			if(rows[0]!== undefined && rows[0]!==null ){
				brokerIP = rows[0].ip;
				console.log("brokerIP: ", brokerIP);
				
				//remember the publishing must also be done in the else
				var destBrokerUrl = "http://" + brokerIP+ ":9002/requestDataPublishing";							
				var parameters = { Topic:Topic,data:data };
				
				
				request({url:destBrokerUrl, method:"POST", json:true, body:parameters}, function(err, response, body) {
					if(err) { 
						console.log(err); 
						return ;
					}
					console.log("response: " + response.statusCode);
					if(response.statusCode == 200){
						return ;
					}else{
						return ;
					}																		
				});
			}
		})
		.catch(err => {
			console.log("Impossibile eseguire la query receiveRewireDataFromSB :" + err);
			return ;
		});	
						
	
return res.json();
});
