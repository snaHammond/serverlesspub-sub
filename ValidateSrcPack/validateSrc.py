import MySQLdb as mariadb
import json
import requests
def main(srcSBName,destSBName,Topic,**kwargs):
    code = -1
    srcSBIP = ''
    TopicV = ''
    res = "{\"res\":"+str(code)+",\"srcSBIP\":\""+srcSBIP+"\",\"Topic\":\""+TopicV+"\"}"
    #first basic check if variables are filled, remember that srcSBName and srcBrokerName are optional
    if Topic:
        #check if the Topic is complete, else we need to and do discovery
        topicSplitted = Topic.split(".")
        #get the srcSBDomain        
        if topicSplitted[0]:
            #confirm is this a valid domain name of a superbroker
            headers = {'content-type':'application/json'}
            r = requests.post( 'http://10.8.0.2:7070/v1/executions/' ,headers=headers, json={"function_id":"1c75a9e3-a446-4e12-a5a9-c6a0561b70b1","input":"{\"SBDomain\":\""+topicSplitted[0]+"\"}"})
            ro = r.json()['result']
            roj = json.loads(ro)    
            resultsDomainExist = roj['output']        
            ##get the domain's existance, have to unmarshall the reponse from confirmSuperBrokerDomain
            resultsDomainExistj = json.loads(resultsDomainExist)
            if resultsDomainExistj['res']==0 and srcSBName:                   
                #get the srcSBDomain using the srcSBName, since the Topic knows the SBDomain, it is assumed that it also know the SBName
                #there use the srcSBName to get the SBIP
                r = requests.post( 'http://10.8.0.2:7070/v1/executions/' ,headers=headers, json={"function_id":"666eea17-660b-4295-9f7b-86c1ade9de81","input":"{\"SBName\":\""+srcSBName+"\"}"})
                ro = r.json()['result']
                roj = json.loads(ro)    
                resultsIP = roj['output']        
                ##get the ip, have to unmarshall the reponse from getSBIP
                resultsIPj = json.loads(resultsIP)  
                if resultsIPj['res']==0:
                    code = 0
                    srcSBIP = resultsIPj['SBIP']
                    TopicV = Topic
                    ##we can return from here                                      
                    res = "{\"res\":"+str(code)+",\"srcSBIP\":\""+srcSBIP+"\",\"Topic\":\""+TopicV+"\"}"
            else:
                #the requestor does not know the domain it wants; as the topic does not have a valid domain attached, eg; test.counter
                #will therefore do a search (discovery) for the topic attaching the domains registered in the fed 1 by 1
                r = requests.post( 'http://10.8.0.2:7070/v1/executions/' ,headers=headers, json={"function_id":"14f50e46-095e-4f98-8fd6-3b785a062bc7","input":"{\"destSBName\":\""+destSBName+"\",\"Topic\":\""+Topic+"\"}"})
                ro = r.json()['result']
                roj = json.loads(ro)    
                resultsIP = roj['output']        
                ##get the ip, have to unmarshall the reponse from getSBIP
                resultsSrcSB = json.loads(resultsIP)  
                if resultsSrcSB['res']==0:
                    code = 0
                    srcSBIP = resultsSrcSB['srcSBIP']
                    TopicV = resultsSrcSB['Topic']                
                    res = "{\"res\":"+str(code)+",\"srcSBIP\":\""+srcSBIP+"\",\"Topic\":\""+TopicV+"\"}"
               
               
       
       
           
       
    return str(res)
   
if __name__ == '__main__':
    main()