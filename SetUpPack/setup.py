import MySQLdb as mariadb
def main(**kwargs):
    res = ""
    try:
        db = mariadb.connect(host="192.168.56.104",    
                         user="federator",        
                         passwd="password",  
                         db="feddb")        
        cur = db.cursor()
       
        cur.execute("CREATE TABLE IF NOT EXISTS superbrokers (superbroker_id INT(11) NOT NULL AUTO_INCREMENT,ip varchar(15), name varchar(100),domain varchar(100),status varchar(100), UNIQUE (ip, name), CONSTRAINT superbrokers_pk PRIMARY KEY (superbroker_id))")
       
        cur.execute("CREATE TABLE IF NOT EXISTS requests (Topic varchar(100), SourceDomain varchar(40), DestDomain varchar(40),  DestBroker varchar(40),  DestTopic varchar(40), UNIQUE(Topic,DestDomain,DestBroker,DestTopic))")
        
        res = "SetUp Completed Successfully"
    except Exception as e:
        res = str(e)
    finally:
        db.close()
       
    return str(res)
   
if __name__ == '__main__':
    main()